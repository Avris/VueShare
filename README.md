# VueShare

Social media share buttons for VueJS

Adapted from and inspired by [shareon.js](https://shareon.js.org) (MIT).
I couldn't use it directly, because window.onload conflicts with SSR.

## Usage

Minimal:

```vue
<Share title="My awesome website"/>
```

With options:

```vue
<Share baseUrl="https://example.com" url="/foo/bar" title="My awesome website" :networks="['mastodon', 'reddit']"/>
```

## Demo

Check out the footers of https://emojiflags.avris.it/ or https://futurysci.avris.it/
